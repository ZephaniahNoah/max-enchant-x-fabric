package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.RespirationEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(RespirationEnchantment.class)
public class RespirationMixin {

    private static final int value = MaxEnchantX.respiration;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
