package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.KnockbackEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(KnockbackEnchantment.class)
public class KnockbackMixin {

    private static final int value = MaxEnchantX.knockback;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
