package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.EfficiencyEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(EfficiencyEnchantment.class)
public class EfficiencyMixin {

    private static final int value = MaxEnchantX.efficiency;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
