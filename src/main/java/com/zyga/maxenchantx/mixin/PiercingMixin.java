package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.PiercingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PiercingEnchantment.class)
public class PiercingMixin {

    private static final int value = MaxEnchantX.piercing;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
