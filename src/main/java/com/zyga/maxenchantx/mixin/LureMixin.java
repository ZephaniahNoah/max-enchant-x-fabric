package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.LureEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(LureEnchantment.class)
public class LureMixin {

    private static final int value = MaxEnchantX.lure;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
