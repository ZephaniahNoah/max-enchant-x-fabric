package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.UnbreakingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(UnbreakingEnchantment.class)
public class UnbreakingMixin {

    private static final int value = MaxEnchantX.unbreaking;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
