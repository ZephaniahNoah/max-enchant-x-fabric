package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.RiptideEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(RiptideEnchantment.class)
public class RiptideMixin {

    private static final int value = MaxEnchantX.riptide;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
