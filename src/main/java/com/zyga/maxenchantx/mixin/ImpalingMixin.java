package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.ImpalingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ImpalingEnchantment.class)
public class ImpalingMixin {

    private static final int value = MaxEnchantX.impaling;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
