package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.DamageEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(DamageEnchantment.class)
public class SharpnessMixin {

    private static final int value = MaxEnchantX.sharpness;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
