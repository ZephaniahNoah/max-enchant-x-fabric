package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.ThornsEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(ThornsEnchantment.class)
public class ThornsMixin {

    private static final int value = MaxEnchantX.thorns;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
