package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.LuckEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(LuckEnchantment.class)
public class LootingMixin {

    private static final int value = MaxEnchantX.looting;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
