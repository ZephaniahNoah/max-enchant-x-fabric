package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.SoulSpeedEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(SoulSpeedEnchantment.class)
public class SoulSpeedMixin {

    private static final int value = MaxEnchantX.soulSpeed;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }

    @Overwrite
    public boolean isTreasure() {
        return false;
    }

    @Overwrite
    public boolean isAvailableForEnchantedBookOffer() {
        return true;
    }

    @Overwrite
    public boolean isAvailableForRandomSelection() {
        return true;
    }
}
