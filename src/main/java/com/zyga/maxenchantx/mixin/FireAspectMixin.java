package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.FireAspectEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(FireAspectEnchantment.class)
public class FireAspectMixin {

    private static final int value = MaxEnchantX.fireAspect;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
