package com.zyga.maxenchantx.mixin;

import net.minecraft.enchantment.MendingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(MendingEnchantment.class)
public class MendingMixin {

    @Overwrite
    public boolean isTreasure() {
        return false;
    }
}
