package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.FrostWalkerEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(FrostWalkerEnchantment.class)
public class FrostWalkerMixin {

    private static final int value = MaxEnchantX.frostWalker;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }

    @Overwrite
    public boolean isTreasure() {
        return false;
    }
}
