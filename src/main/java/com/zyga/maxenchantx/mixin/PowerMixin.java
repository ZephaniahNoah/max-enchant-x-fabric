package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.PowerEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PowerEnchantment.class)
public class PowerMixin {

    private static final int value = MaxEnchantX.power;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
