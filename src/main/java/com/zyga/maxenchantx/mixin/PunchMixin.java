package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.PunchEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(PunchEnchantment.class)
public class PunchMixin {

    private static final int value = MaxEnchantX.arrowKnockback;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
