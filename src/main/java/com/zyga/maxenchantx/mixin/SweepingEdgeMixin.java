package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.SweepingEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(SweepingEnchantment.class)
public class SweepingEdgeMixin {

    private static final int value = MaxEnchantX.sweepingEdge;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
