package com.zyga.maxenchantx.mixin;

import com.zyga.maxenchantx.MaxEnchantX;
import net.minecraft.enchantment.LoyaltyEnchantment;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Overwrite;

@Mixin(LoyaltyEnchantment.class)
public class LoyaltyMixin {

    private static final int value = MaxEnchantX.loyalty;

    @Overwrite
    public int getMaxLevel() {
        return value;
    }
}
