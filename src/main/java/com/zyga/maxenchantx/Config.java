package com.zyga.maxenchantx;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Config {

    private static final String configDirectory = "config/" + MaxEnchantX.MODID + "/";
    private static final Map<String, String> values = new HashMap<String, String>();
    private String config;

    public Config(String name) {
        config = name + ".ini";
    }

    public int getInt(String name) {
        return getInt(name, 10);
    }

    public int getInt(String name, Integer defaultValue) {
        if (values.containsKey(name)) {
            return Integer.parseInt(values.get(name));
        }
        values.put(name, defaultValue.toString());
        return defaultValue;
    }

    void saveConfig() {
        checkFolder();
        FileWriter writer;
        String val = null;
        try {
            writer = new FileWriter(new File(configDirectory + config));
            for (Entry<String, String> pair : values.entrySet()) {
                val = pair.getKey();
                writer.write(val + "=" + pair.getValue() + "\n");
            }
            writer.close();
        } catch (IOException e) {
            if (val == null) {
                System.out.println("Failed to save " + config);
            } else {
                System.out.println("Failed to save value for " + val);
            }
            System.out.println("Because: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private static void checkFolder() {
        File folder = new File(configDirectory);
        if (!folder.exists()) {
            folder.mkdir();
        }
    }

    public void load() {
        checkFolder();
        File file = new File(configDirectory + config);
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = "";
            while ((line = reader.readLine()) != null) {
                line = line.replaceAll(" ", "");
                if (!line.startsWith("#") && line.contains("=")) {
                    String[] split = line.split("=");
                    values.put(split[0], split[1]);
                }
            }
            reader.close();
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
